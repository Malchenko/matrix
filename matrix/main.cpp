#include <iostream>
#include "matrix.h"

using namespace std;

Matrix readMatrix() {
    cout << "Введите размеры матрицы: ";
    int i, j;
    cin >> i >> j;
    Matrix out(i, j);
    cout << "Введетие значения матрицы:\n";
    for (int i_i = 0; i_i < i; i_i++) {
        for (int i_j = 0; i_j < j; i_j++) {
            int x;
            cin >> x;
            out.set(i_i, i_j, x);
        }
    }

    cout << "\n";
    return out;
}
int main()
{
    cout << "Добро пожаловать!\n\n";

    int number = 0;
    while (true) {
        cout << "Выберите пункт меню:\n\n"
                "1) Сумма матриц\n"
                "2) Разница матриц\n"
                "3) Произведение матриц\n"
                "4) Равенство матриц\n"
                "5) Прозиведение матрицы на число\n"
                "7) Выход\n";

        cin >> number;

        switch(number) {
        case 1:
        {
            Matrix fm = readMatrix();
            Matrix sm = readMatrix();
            bool flag = false;
            Matrix out = Matrix::sum(fm, sm, flag);
            if (!flag) {
                cout << "Сложение матриц невозможно!\n";
                break;
            }
            cout << "Сумма матриц:\n";
            for (int i = 0; i < out.rowsCount(); i++) {
                for (int j = 0; j < out.columnsCount(); j++) {
                    cout << (out.get(i, j)) << " ";
                }
                cout << "\n";
            }

            break;
        }
        case 2:
        {
            Matrix fm = readMatrix();
            Matrix sm = readMatrix();
            bool flag = false;
            Matrix out = Matrix::diff(fm, sm, flag);
            if (!flag) {
                cout << "Разность матриц невозможно!\n";
                break;
            }
            cout << "Разность матриц:\n";
            for (int i = 0; i < out.rowsCount(); i++) {
                for (int j = 0; j < out.columnsCount(); j++) {
                    cout << (out.get(i, j)) << " ";
                }
                cout << "\n";
            }

            break;
        }
            break;
        case 3:
        {
            Matrix fm = readMatrix();
            Matrix sm = readMatrix();
            bool flag = false;
            Matrix out = Matrix::mul(fm, sm, flag);
            if (!flag) {
                cout << "Произведение матриц невозможно!\n";
                break;
            }
            cout << "Произведение матриц:\n";
            for (int i = 0; i < out.rowsCount(); i++) {
                for (int j = 0; j < out.columnsCount(); j++) {
                    cout << (out.get(i, j)) << " ";
                }
                cout << "\n";
            }

            break;
        }
        case 4:
        {
            Matrix fm = readMatrix();
            Matrix sm = readMatrix();

            if (fm==sm)
            {
                cout << "Матрицы равны\n";
            }
            else
            {
                cout << "Матрицы не равны или разного размера\n";
            }


            break;
        }
        case 5:
        {
            Matrix fm = readMatrix();
            int v = 1;
            cout << "Введите число, на которое хотите умножить: ";
            cin >> v;

            Matrix out = Matrix::mul(fm, v);
            for (int i = 0; i < out.rowsCount(); i++) {
                for (int j = 0; j < out.columnsCount(); j++) {
                    cout << (out.get(i, j)) << " ";
                }
                cout << "\n";
            }
        }

        case 7:
            return 0;

        default:
            cout << "Введите возможное значение\n";
            number = 0;
            break;
        }
    }

    return 0;
}
