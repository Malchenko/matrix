#pragma once

#include <vector>

class Matrix
{
public:
    Matrix(int row, int column, int fill = 0);

private:
    std::vector<std::vector<int>>
    _matrix;

public:
    int rowsCount() const
    {
        return _matrix.size();
    }
    int columnsCount() const
    {
        if (rowsCount() > 0)
        {
            return _matrix[0].size();
        }
        else
        {
            return 0;
        }
    }
    int get(int i, int j);
    void set(int i, int j, int v);

    static Matrix mul(Matrix f, Matrix s, bool &flag) {
        if (f.columnsCount() != s.rowsCount())
        {
            flag = false;
            return Matrix(0, 0, 0);
        }

        Matrix out(f.rowsCount(), s.columnsCount(), 0);

        for (int i = 0; i < f.rowsCount(); i++)
        {
            for (int j = 0; j < s.columnsCount(); j++)
            {
                for (int k = 0; k < f.columnsCount(); k++)
                {
                    out.set(i, j, out.get(i, j) + f.get(i, k) * s.get(k, j));
                }
            }
        }




        flag = true;
        return out;
    }

    static Matrix mul(Matrix f, int v) {

        for (int i = 0; i < f.rowsCount(); i++)
        {
            for (int j = 0; j < f.columnsCount(); j++)
            {
                f.set(i, j, (f.get(i, j) * v));
            }
        }



        return f;
    }

    static Matrix diff(Matrix f, Matrix s, bool &flag) {
        if (f.columnsCount() != s.columnsCount() || f.rowsCount() != s.rowsCount())
        {
            flag = false;
            return Matrix(0, 0, 0);
        }

        Matrix out(f.rowsCount(), f.columnsCount());
        for (int i = 0; i < f.rowsCount(); i++) {
            for (int j = 0; j < f.columnsCount(); j++) {
                out.set(i, j, f.get(i, j) - s.get(i, j));
            }
        }

        flag = true;
        return out;
    }
    static Matrix sum(Matrix f, Matrix s, bool &flag)  {
        if (f.columnsCount() != s.columnsCount() || f.rowsCount() != s.rowsCount())
        {
            flag = false;
            return Matrix(0, 0, 0);
        }

        Matrix out(f.rowsCount(), f.columnsCount());
        for (int i = 0; i < f.rowsCount(); i++) {
            for (int j = 0; j < f.columnsCount(); j++) {
                out.set(i, j, f.get(i, j) + s.get(i, j));
            }
        }

        flag = true;
        return out;
    }
    bool operator ==(Matrix mat);
};
