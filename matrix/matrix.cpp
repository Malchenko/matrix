#include "matrix.h"

using namespace std;

Matrix::Matrix(int row, int column, int fill)
{
    _matrix = vector<vector<int>>(row, vector<int>(column, fill));
}

bool Matrix::operator == (Matrix mat)
{
    if (rowsCount() != mat.rowsCount() || columnsCount() != mat.columnsCount())
    {
        return false;
    }

    for (int i = 0; i < rowsCount(); i++)
    {
        for (int j = 0; j < columnsCount(); j++)
        {
            if (get(i, j) != mat.get(i, j))
            {
                return false;
            }
        }
    }

    return true;
}

int Matrix::get(int i, int j) {
    return _matrix[i][j];
}

void Matrix::set(int i, int j, int v) {
    _matrix[i][j] = v;
}

