#include <QString>
#include <QtTest>

#include </home/inligy/work/matrix/matrix/matrix.cpp>

class UnitTests : public QObject
{
    Q_OBJECT

public:
    UnitTests();

private Q_SLOTS:
    void testCaseDiff();
    void testCaseSum();
    void testCaseMul();
    void testCaseEq();
};

UnitTests::UnitTests()
{
}

void UnitTests::testCaseDiff() {
    Matrix fm(1, 2, 0);
    fm.set(0, 0, 10);
    fm.set(0, 1, 2);
    Matrix sm(1, 2, 3);
    bool flag = true;
    Matrix out = Matrix::diff(fm, sm, flag);
    QCOMPARE(flag, true);
    QCOMPARE(out.columnsCount(), 2);
    QCOMPARE(out.rowsCount(), 1);
    QCOMPARE(out.get(0, 0), 7);
    QCOMPARE(out.get(0, 1), -1);
}

void UnitTests::testCaseSum() {
    Matrix fm(1, 2, 0);
    fm.set(0, 0, 10);
    fm.set(0, 1, 2);
    Matrix sm(1, 2, 3);
    bool flag = true;
    Matrix out = Matrix::sum(fm, sm, flag);
    QCOMPARE(flag, true);
    QCOMPARE(out.columnsCount(), 2);
    QCOMPARE(out.rowsCount(), 1);
    QCOMPARE(out.get(0, 0), 13);
    QCOMPARE(out.get(0, 1), 5);
}

void UnitTests::testCaseMul()
{
    Matrix fm(1, 2, 0);
    fm.set(0, 0, 10);
    fm.set(0, 1, 2);
    Matrix sm(2, 1, 1);
    bool flag = true;
    Matrix out = Matrix::mul(fm, sm, flag);
    QCOMPARE(flag, true);
    QCOMPARE(out.columnsCount(), 1);
    QCOMPARE(out.rowsCount(), 1);
    QCOMPARE(out.get(0, 0), 12);
}

void UnitTests::testCaseEq()
{
    QCOMPARE(Matrix(2, 1, 1) == Matrix(2, 1, 1), true);
}

QTEST_APPLESS_MAIN(UnitTests)

#include "unittests.moc"
